
import sys
sys.path.append('..')

from ml_engine.src.model import LSTM
from ml_engine.src.model_trainer import ModelTrainer

from torchtext import datasets
from torchtext import data
import torch


def test_sentiment_classification_wt_embeddings():
    # This is not a great integration test because
    # the dataset is so large and hence the training and
    # testing time is quite long.

    print('Running integration tests... This may take a while')

    params = {
        'pre_trained_emb': True,
        'freeze_pre_trained_layer': True,
        'text_label': 'text',
        'class_label': 'label',
        'learning_rate': 0.005,  # 0.00005,
        'num_epochs': 20,
        'batch_size': 128,
        'pretrained_emb_file': 'glove.6B.100d'
    }

    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

    TEXT = data.Field(tokenize='spacy')
    LABEL = data.LabelField(dtype=torch.float)

    train_data, test_data = datasets.IMDB.splits(TEXT, LABEL)

    train_data, valid_data = train_data.split()

    TEXT.build_vocab(train_data,
                     max_size=10000,
                     vectors=params['pretrained_emb_file'])

    LABEL.build_vocab(train_data)

    train_iterator, valid_iterator, test_iterator = data.BucketIterator.splits(
        (train_data, valid_data, test_data),
        batch_size=params['batch_size'],
        device=device)

    vocab_size = len(TEXT.vocab.itos)

    model = LSTM(
        vocab_size=vocab_size,
        embedding_dim=100,
        hidden_dim=64,
        output_dim=1,
        bidirectional=True,
        dropout_linear=0.5,
        dropout_rnn=0.3,
        save_dir=None
    )

    if params['pre_trained_emb']:
        pre_trained_embeddings = TEXT.vocab.vectors
        model.embed.weight.data.copy_(pre_trained_embeddings)
        if params['freeze_pre_trained_layer']:
            model.embed.weight.requires_grad = True

    model_trainer = ModelTrainer(model,
                                 train_iterator,
                                 valid_iterator,
                                 device,
                                 configs=params)

    (model, history_train, history_val) = model_trainer.train()

    assert (history_val[-1]['accuracy'] >= 0.8)




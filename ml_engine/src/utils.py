

from ml_engine.src.model import LSTM
import torch


def load_model_checkpoint(path, device):
    """
    Load the saved state of the LSTM model.

    The model is rebuild here using the saved
    model_state_dict.

    # Parameters
        path: str, full path to the model checkpoint file

    # Returns
        model: pytorch model, with weights and architecture
            identical to the saved model.
        text_vocab: torchtext vocab, vocab objects for texts
        label_vocab : torchtext vocab, vocab objects for the labels

    """
    # Load the checkpoint.
    checkpoint = torch.load(path)

    # Create an LSTM model instance.
    # save_dir is None because the loaded
    # model here should only be used for prediction

    model = LSTM(
        vocab_size = checkpoint['vocab_size'],
        embedding_dim = checkpoint['embedding_dim'],
        hidden_dim = checkpoint['hidden_dim'],
        output_dim = checkpoint['output_dim'],
        bidirectional = checkpoint['bidirectional'],
        dropout_linear = checkpoint['dropout_linear'],
        dropout_rnn = checkpoint['dropout_rnn'],
        save_dir = None
    )

    # Set the weights of the model
    model.load_state_dict(checkpoint['model_state_dict'])

    # Send the model to the correct device
    model = model.to(device)

    return (model,
            checkpoint['text_vocab'],
            checkpoint['label_vocab'])

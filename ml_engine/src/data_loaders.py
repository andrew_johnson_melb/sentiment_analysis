

import spacy
import torch
from torchtext import data
from torchtext.data import TabularDataset
from torchtext.data import Iterator
from torchtext.data import Field
import re
import pandas as pd

nlp = spacy.load('en')

# Parameters for tokenizer.
MAX_CHARS = 10000
IGNORE_CHARS = r"[\*\"“”\n\\…\+\-\/\=\(\)‘•:\[\]\|’\!;]"

def tokenizer(text):
    # Custom tokenizer, remove bad characters.
    text = re.sub(IGNORE_CHARS, " ", str(text))
    text = re.sub(r"[ ]+", " ", text)
    text = re.sub(r"\!+", "!", text)
    text = re.sub(r"\,+", ",", text)
    text = re.sub(r"\?+", "?", text)
    if (len(text) > MAX_CHARS):
        text = text[:MAX_CHARS]
    return [x.text for x in nlp.tokenizer(text) if x.text != " "]


def build_data_sets(data_dir,
                    train_file,
                    val_file,
                    test_file,
                    max_size_vocab=10000,
                    min_word_freq=1,
                    label_name='label',
                    text_name='text',
                    pretrained_emb_file="glove.6B.100d"):

    """
    Reads in input data and creates a vocab from the text
    in the training set.

    A datasets instance is generated separately for the
    input train, validation, and test files.
    
    # Parameters
        data_dir: str, root path to data
        train_file: str, name of training file
        val_file: str, name of validation data file
        test_file: str, name of test file.
        max_size_vocab: int, max number of words to include 
                in text vocab (default, 10000)
        min_word_freq: Smallest frequency of words to include 
            in vocab (default, 1)
        label_name: str, name to give class label 
            data (default, 'label')
        text_name: str, name to given text data (default, 'text')
        pre-trained_emb_file: name of pre-trained word
            embeddings (default, "glove.6B.100d")

    # Returns
        dict with keys 'train', 'val', 'test'. The dict values 
            are the corresponding dataset.
        vocab object, text vocab build from the training data
        vocab object, label vocab build from the training data

    """

    TEXT = data.Field(tokenize=tokenizer, lower=True)
    LABEL = data.LabelField(dtype=torch.float)

    train_fields = [(label_name, LABEL), (text_name, TEXT)]

    train_dataset, val_dataset = data.TabularDataset.splits(
        path=data_dir,
        train=train_file,
        validation=val_file,
        format='csv',
        skip_header=True,
        fields=train_fields)

    test_fields = [("id", None), (text_name, TEXT)]

    test_dataset = TabularDataset(
        path=data_dir + test_file,
        format='csv',
        skip_header=True,
        fields=test_fields)

    # Build a vocab from the training data.
    # The vocab will define a unique mapping from
    # token -> integer
    # We will also load a pre-trained word embedding.

    TEXT.build_vocab(train_dataset,
                     max_size=max_size_vocab,
                     min_freq=min_word_freq,
                     vectors=pretrained_emb_file)

    LABEL.build_vocab(train_dataset)

    # To predict new sentences we need to store
    # the mappings (vocab) for both
    # the text data and the label data.

    text_vocab = TEXT.vocab
    label_vocab = LABEL.vocab

    # Package the datasets into a dict
    datasets = {
        'train' : train_dataset,
        'val' : val_dataset,
        'test' : test_dataset
        }

    return (datasets,
            text_vocab,
            label_vocab)


def build_data_batches(datasets,
                       device,
                       batch_size = 32):

    """
    pre-precessing the input datasets and divides them into
    batches.

    The data is shuffled and bucketed according the sequence
    length to minimize the amount of padding needed.
    The test data will by default have a batch size of 1 as
    this will not be used to training. 

    # Parameters
        datasets: dict, dataset dict with keys 'train', 'val', 
            and 'test'. The values of the dict should be the 
            corresponding torch text TabularDataset object.
        device: str, the device to place the data on.
        batch_size: the size of the batches to use for the training
            and the validation data. 

    # Returns
        iterator, batch iterator for the training data 
        iterator, batch iterator for the validation data
        iterator, batch iterator for the test data .
    """

    # Unpack datasets
    train_dataset = datasets['train']
    val_dataset = datasets['val']
    test_dataset = datasets['test']

    train_iterator, valid_iterator = data.BucketIterator.splits(
        (train_dataset, val_dataset),
        batch_size=batch_size,
        device=device,
        sort_key=lambda x: len(x.text),
        sort=True,
        sort_within_batch=False)

    # Create basic iterator for test set
    # to make it simple to track, we can set the
    # batch size to 1, and turn shuffle off

    test_iterator = Iterator(test_dataset,
                             batch_size=1,
                             device=device,
                             shuffle=False,
                             sort=False,
                             sort_within_batch=False,
                             repeat=False)

    return (train_iterator,
            valid_iterator,
            test_iterator)


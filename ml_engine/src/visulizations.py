
import pandas as pd
import matplotlib
matplotlib.use('TkAgg')
import matplotlib.pylab as plt

METRICS = ['accuracy', 'loss']


def plot_training_history(history_train, history_val,
                          plot_dir='../plots/' ):

    """
    Create a model complexity curve (loss vs training epoch)
    for the training and validation data. The plot
    will be saved in 'plot_dir'

    """

    train_history = (pd.DataFrame(history_train)
                     .set_index('epoch', drop=True))

    val_history = (pd.DataFrame(history_val)
                   .set_index('epoch', drop=True))

    # Create a separate plot for each metric.

    for metric in METRICS:

        plt.figure(figsize=(12,8))
        plt.title('Model Complexity Curve', fontsize=16)
        plt.plot(train_history[metric].index,
                 train_history[metric].values,
                 label='Training Set')

        plt.plot(val_history[metric].index,
                 val_history[metric].values,
                 label='Validation Set')
        plt.legend()
        plt.ylabel(metric, fontsize=16)
        plt.xlabel('Epochs', fontsize=16)
        plt.savefig(plot_dir + 'complexity_curve_{}.pdf'.format(metric))
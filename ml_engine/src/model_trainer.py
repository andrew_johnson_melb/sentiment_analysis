

import torch
import torch.nn as nn
import torch.optim as optim
import logging

logger = logging.getLogger(__name__)


class ModelTrainer:
    """
    Class for training a pytorch model.

    This class takes in a pytorch model and train/validation 
    batch loaders. Using these inputs the pytorch model is trained
    and it's loss and accuracy is evaluated on the validation data.
    The default optimizer is Adam's and the criterion/loss is
    Binary Cross Entropy with Logits.

    # Parameters
        model: pytorch model, the model to be trained, should be 
            instance of the LSTM class. 
        train_loader: torch text BucketIterator, a batch 
            generator for the training data.
        val_loader:  torch text BucketIterator, a batch 
            generator for the validation data.
        device: str, device to run training on, options 
            are "cpu" or "gpu"
        configs: dict, defines model configuration parameters
    
    # config Parameters
        num_epochs: int, The number of epochs to train 
            the model on. To optimizer this number check 
            the model complexity plots.
        input_label: str, name of text / review data column for 
            torchtexts data loaders.
        class_label: str, name of class/ label data column for 
            torchtexts data loaders.

    """

    def __init__(self, model, train_loader, val_loader, device, configs):

        self.model = model
        self.device = device
        self.num_epochs = configs['num_epochs']
        self.input_label = configs['text_label']
        self.class_label = configs['class_label']
        self.train_loader = train_loader
        self.val_loader = val_loader

        # Create optimizer and set criterion to BCE with logits
        self.criterion = nn.BCEWithLogitsLoss()
        self.optimizer = optim.Adam(
            model.parameters(),
            lr=configs['learning_rate'])

        # Send model to device
        self.model.to(self.device)

        # Compute the size of the training and validation sets.
        self.train_size = len(self.train_loader.dataset)
        self.val_size = len(self.val_loader.dataset)

        # Create an object to store all training/val results
        self.loss_history_train = []
        self.loss_history_val = []

        assert type(self.num_epochs) is int, "num_epochs needs to be an integer"


    def train(self):
        """
        Train the model for epochs set by 'config['num_epochs']'
        """
        for epoch_idx in range(self.num_epochs):
            self._train_model(epoch_idx)
            self._val_model(epoch_idx)

        return self.model, self.loss_history_train, self.loss_history_val


    def _train_model(self, epoch_idx):
        """
        Train the model on epoch 'epoch_idx'
        """
        self.model.train()

        # To track training loss and accuracy.
        running_loss = 0.0
        running_corrects = 0

        for batch_idx, data in enumerate(self.train_loader):

            inputs = getattr(data, self.input_label)
            classes = getattr(data, self.class_label)

            # Get the size of the batch, may vary
            batch_size = inputs.shape[1]

            # Create a new init hidden state
            (hid, cell) = self.model.init_hidden(batch_size)
            hidden = (hid.to(self.device), cell.to(self.device))

            # Transfer the batch data to the correct device
            inputs, classes = inputs.to(self.device), classes.to(self.device)

            # Zero the parameter gradients
            self.optimizer.zero_grad()

            # Forward pass
            output = self.model.forward(inputs, hidden)
            output = output.squeeze(1)
            loss = self.criterion(output, classes)

            # Backward pass (since this is the training fn)
            loss.backward()
            self.optimizer.step()

            running_loss += loss.item() * inputs.size(0)

            # Transform the output into the range [0,1]
            # using the sigmoid function.
            rounded_output = torch.round(torch.sigmoid(output))
            running_corrects += torch.sum((rounded_output == classes))

        epoch_loss = running_loss / self.train_size
        epoch_acc = 100.0 * (running_corrects.float().item() / self.train_size)

        logger.info( '#' * 50)
        logger.info('Epoch {} / {}: Set = Train'.format(epoch_idx, self.num_epochs))
        logger.info('Loss = {:.4f}'.format(epoch_loss))
        logger.info('Accuracy = {:.4f} %'.format(epoch_acc))

        self.loss_history_train.append({
            'accuracy': epoch_acc,
            'loss': epoch_loss,
            'epoch' : epoch_idx})


    def _val_model(self, epoch_idx):
        """
        Validate the model on epoch 'epoch_idx'
        """
        self.model.eval()

        running_loss = 0.0
        running_corrects = 0

        # Turn off gradient tracking for when evaluating the model.
        with torch.no_grad():

            for batch_idx, data in enumerate(self.val_loader):

                inputs = getattr(data, self.input_label)
                classes = getattr(data, self.class_label)
                batch_size = inputs.shape[1]

                inputs, classes = inputs.to(self.device), classes.to(self.device)

                # Create a new init hidden state
                (hid, cell) = self.model.init_hidden(batch_size)
                hidden = (hid.to(self.device), cell.to(self.device))

                output = self.model.forward(inputs, hidden)
                output = output.squeeze(1)
                loss = self.criterion(output, classes)

                running_loss += loss.item() * inputs.size(0)

                rounded_output = torch.round(torch.sigmoid(output))
                running_corrects += torch.sum((rounded_output == classes))

        epoch_loss = running_loss / self.val_size
        epoch_acc = 100.0 * (running_corrects.float().item() / self.val_size)

        logger.info( '#' * 50)
        logger.info('Epoch {} / {}: Set = Validate'.format(epoch_idx, self.num_epochs))
        logger.info('Loss = {:.4f}'.format(epoch_loss))
        logger.info('Accuracy = {:.4f} %'.format(epoch_acc))

        self.loss_history_val.append({
            'accuracy': epoch_acc,
            'loss': epoch_loss,
            'epoch' : epoch_idx})



import torch
import torch.nn as nn


class LSTM(nn.Module):
    """
    Class for a creating a basic LSTM pytorch model
    
    The input to this model should be a batch of reviews, where 
    the reviews are encoded as a sequence of integers. 
    The output of this model is the raw (logits) output
    from the final dense linear layer.

    # Parameters
        vocab_size: int, The number of unique words/tokens contained 
            in text vocabulary.
        save_dir: str, The full path to save the model too.
        embedding_dim: int, The size of the word embeddings space. 
            When using a pre-trained network this value should match
            it's dimensions (for the GLOVE network it's 100).
        hidden_dim: The output dimensions of the LSTM network.
        output_dim: int, The output dimensions from the linear layer. 
            For binary classification this should be set to 1.
        bidirectional: bool, whether or not to use a Bidirectional LSTM 
        dropout_linear: float [0-1], probability of dropout for the output 
            of the linear layer.
        dropout_rnn: probability of dropout inside the LSTM network. This
            parameter only has an effect when we have layers of LSTM networks.

    """

    DEFAULT_N_LAYERS = 1

    def __init__(self,
                 vocab_size,
                 embedding_dim,
                 hidden_dim,
                 output_dim,
                 bidirectional,
                 save_dir,
                 dropout_linear=0.5,
                 dropout_rnn=0.3):

        super().__init__()

        # Set default number of layers .

        self.n_layers = 1
        self.bidirectional = bidirectional
        self.hidden_dim = hidden_dim
        self.save_dir = save_dir
        self.vocab_size = vocab_size
        self.embedding_dim = embedding_dim
        self.output_dim = output_dim
        self.dropout_linear = dropout_linear
        self.dropout_rnn = dropout_rnn

        # Default size of input for linear layer.
        fc_input_dim = hidden_dim

        if self.bidirectional:
            # For a bi-directional LSTM the input dims
            # for the linear layer are 2 * hidden dims
            # This is because we stack the forward and
            # backwards passes together.
            fc_input_dim = fc_input_dim * 2
            self.n_layers = 2

        # Create embedding layer.
        self.embed = nn.Embedding(vocab_size,
                                  embedding_dim)

        # Create LSTM cell
        self.rnn = nn.LSTM(embedding_dim,
                           hidden_dim,
                           bidirectional=bidirectional,
                           dropout=dropout_rnn)

        # Final linear layer
        self.fc = nn.Linear(fc_input_dim, output_dim)

        self.dropout = nn.Dropout(dropout_linear)


    def forward(self, x, hidden):
        """
        Forward pass on network
        """

        # Create work embeddings
        x = self.embed(x)

        # pass embeddings to RNN layer,
        # hidden dim  = [1 * batch * hidden state]
        output, (hidden, cell) = self.rnn(x, hidden)

        if self.bidirectional:
            # We need to stack the forward and backwards layers
            backwards_hidden = hidden[-1 ,: ,:]
            forwards_hidden = hidden[-2 ,: ,:]

            final_hidden = torch.cat(
                (forwards_hidden, backwards_hidden),
                dim=1)
        else:
            final_hidden = hidden.squeeze(0)

        # Apply dropout to the output of RNN
        final_hidden = self.dropout(final_hidden)

        # Pass RNN output in linear layer.
        # will apply sigmoid activation within training loop.
        out = self.fc(final_hidden)
        return out


    def init_hidden(self, batch_size):
        # Create a hidden state for the first call to RNN network
        return (
            torch.zeros(self.n_layers, batch_size, self.hidden_dim),
            torch.zeros(self.n_layers, batch_size, self.hidden_dim))


    def save_state(self, extra_state_info):
        """
        Save the current state of the model.

        This saved model state should only be used
        for generating predictions. To continue training
        the state of the optimizer should be saved.

        # Parameters
            extra_state_info: dict, contains extra state
                information that is needed to reuse the model.
        
        """

        model_state = {
            'model_state_dict': self.state_dict(),
            'vocab_size' : self.vocab_size,
            'embedding_dim' : self.embedding_dim,
            'hidden_dim' : self.hidden_dim,
            'output_dim' : self.output_dim,
            'bidirectional' : self.bidirectional,
            'dropout_linear' : self.dropout_linear,
            'dropout_rnn' : self.dropout_rnn}

        model_state.update(extra_state_info)
        torch.save(model_state, self.save_dir)



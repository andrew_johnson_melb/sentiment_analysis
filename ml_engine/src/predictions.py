
from ml_engine.src.data_loaders import tokenizer

import pandas as pd
import torch
import numpy as np


def predict_test_set(model, test_loader, label_vocab,
                    text_vocab, text_label, data_dir,
                    sentiment_map, device,
                    file_name='test_preds.csv'):
    """
    Create sentiment predictions for the test data.

    Will will reconstruct the original text here
    and write the sentiment predictions next to it.

    Note, batch size needs to be set to 1.

    """

    # Set the model to eval mode
    model.eval()

    print ('Using the sentiment map', sentiment_map)

    COLUMN_NAMES = ['id', 'text', 'predicted_label', 'pred_sentiment']

    # We can invert the vocab word to index mapping
    # to recover the index to word mapping.

    idx_to_text_map = text_vocab.itos
    idx_to_label_map = label_vocab.itos

    # Iterate over the entire dataset.
    # at each step we will collect the results.

    test_results = []
    for idx, example in enumerate(test_loader):

        inputs = getattr(example, text_label)

        # Init a hidden state.
        (hid, cell) = model.init_hidden(batch_size=1)
        hidden = (hid.to(device), cell.to(device))

        # Get the model predictions
        out = model.forward(inputs, hidden)

        # Calculate the probability.
        prob = torch.sigmoid(out)

        # Round to get the highest prob class.
        pred_binary = torch.round(prob).item()

        # Use the inverse label map
        # to recover the original label
        # in the text file.

        pred_label = idx_to_label_map[int(pred_binary)]
        pred_in_text = sentiment_map[pred_label]

        # Reconstruct the text data.
        text = [idx_to_text_map[tok] for tok in inputs.squeeze(1).cpu().numpy()]
        text = ' '.join(text)

        test_results.append(
            (idx, text, pred_label, pred_in_text)
        )

    # Save the results.
    test_results = pd.DataFrame(test_results, columns=COLUMN_NAMES)
    test_results = test_results.set_index('id', drop=True)
    test_results.to_csv(data_dir + file_name)


def predict_raw_string(model, raw_text, label_vocab, text_vocab, sentiment_map,
                       device):
    """
    Forecast the sentiment of the 'raw_text' using
    the input pytorch model.

    # Parameters
        model: pytorch LSTM model
        test: str, raw text to predict
        label_vocab: torchtext vocab object for the labels
        text_vocab: torchtext vocab object for the text
        sentiment_map: dict, map from binary labels to text labels.


    # Returns
        dict, binary prediction and
        probabilities for each label.

    """
    print ('using device {} for prediction'.format(device))
    # Set the model to eval mode
    model.eval()

    # Find the 'positive', 'negative' labels for
    # the numeric predictions from the model.

    label_1 = sentiment_map[label_vocab.itos[1]]
    label_0 = sentiment_map[label_vocab.itos[0]]

    # Vectorize the input raw text.

    tokens = tokenizer(raw_text)
    numeric_review = np.asarray([text_vocab.stoi[tok] for tok in tokens])
    input_review = torch.from_numpy(numeric_review).unsqueeze(1)

    input_review = input_review.to(device)

    # Get the model prediction. This assumes output
    # is raw, i.e., it has not been normalized by sigmoid.

    # Create a new init hidden state
    (hid, cell) = model.init_hidden(batch_size=1)
    hidden = (hid.to(device), cell.to(device))

    out = model.forward(input_review, hidden)
    prob = torch.sigmoid(out)
    outcome = int(torch.round(prob).item())

    # prob gives the probability of the numeric output 1
    # since it's binary classification, the probability of
    # numeric 0 is 1 - prob

    prob_1 = prob.item()
    prob_0 = 1.0 - prob_1

    # We can now map the final outcome to the 'text' prediction
    # using the sentiment map.

    prediction = sentiment_map[label_vocab.itos[outcome]]

    return ({
        'Predicted Sentiment': prediction,
        'Prob' + label_1: round(prob_1, 3),
        'Prob' + label_0: round(prob_0, 3)
    })

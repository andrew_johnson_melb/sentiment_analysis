
import sys
sys.path.append('..')

from ml_engine.src.utils import load_model_checkpoint
from ml_engine.src.predictions import predict_raw_string

import yaml
import argparse
import torch


EXAMPLE_REVIEW = "That's right....the red velvet cake.....ohhh this stuff is so good."


def argument_parser():

    parser = argparse.ArgumentParser(
        description='predict sentiment of input raw text',)
    parser.add_argument('--text', type=str,
                        help='Input raw text which will be analyzed.',
                        default=EXAMPLE_REVIEW)
    parser.add_argument('--checkpoint_path', type=str,
                        help='path to the saved model',
                        default='../models/checkpoint.pt')
    parser.add_argument('--sentiment_map', type=str,
                        help='path to the sentiment_map',
                        default='../data/sentiment_map.yml')
    parser.add_argument('--gpu', type=bool, default=True,
            help='Whether or not to use the GPU.')
    args = parser.parse_args()
    return args


def main(args, device):

    # Load the sentiment map:
    with open(args.sentiment_map, 'r') as stream:
        sentiment_map = yaml.load(stream)

    model, text_vocab, label_vocab = load_model_checkpoint(
        path=args.checkpoint_path, device=device)

    model.to(device)

    # make the predictions.
    predictions = predict_raw_string(model, args.text, label_vocab, text_vocab, sentiment_map, device)

    print (predictions)


if __name__ == '__main__':
    # Load the input params
    args = argument_parser()
        # Set the correct device
    if args.gpu:
        device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
        print('Using device {}'.format(device))
    else:
        device = torch.device("cpu")
        print('Using device {}'.format(device))

    main(args, device)


import sys
sys.path.append('..')

from ml_engine.src.data_loaders import build_data_sets
from ml_engine.src.data_loaders import build_data_batches
from ml_engine.src.model import LSTM
from ml_engine.src.model_trainer import ModelTrainer
from ml_engine.src.visulizations import plot_training_history
from ml_engine.src.predictions import predict_test_set

import torch
import argparse
import yaml
import logging


def parse_arguments():
    parser = argparse.ArgumentParser(
        description='Train LSTM sentiment model')
    parser.add_argument('--data_root', type=str,
                        help='base path to the training data',
                        default='../data/')
    parser.add_argument('--train_file', type=str,
                        help='file name of training data',
                        default='train.csv')
    parser.add_argument('--val_file', type=str,
                        help='file name of validation data',
                        default='validation.csv')
    parser.add_argument('--test_file', type=str,
                        help='base path to the training data',
                        default='test.csv')
    parser.add_argument('--save_model_dir', type=str,
                        help='The dir where the model will be saved',
                        default='../models/checkpoint.pt')
    parser.add_argument('--sentiment_map', type=str,
                        help='path to the sentiment_map',
                        default='../data/sentiment_map.yml')
    parser.add_argument('--gpu', type=bool, default=True,
                        help='Whether or not to use the GPU for training')
    parser.add_argument('--config', type=str,
                        help='Path to config file storing configuration params',
                        default='../ml_engine/config/config.yml')
    parser.add_argument('--log_dir', type=str,
                        help='Path to out the logs',
                        default='../logs/train.log')

    args = parser.parse_args()
    return args


def main(args, configs, device):

    # Load the sentiment map. This is required so we
    # can output a 'positive' or 'negative' prediction
    # when forecasting the results on the test set
    # (rather than binary values)

    with open(args.sentiment_map, 'r') as stream:
        sentiment_map = yaml.load(stream)

    # Build datasets for the train/val and testing sets.
    # and build a vocab using the training data only.

    (datasets, text_vocab, label_vocab) = build_data_sets(
         data_dir=args.data_root,
         train_file=args.train_file,
         val_file=args.val_file,
         test_file=args.test_file,
         label_name=configs['class_label'],
         text_name=configs['text_label'],
         pretrained_emb_file=configs['pretrained_emb_file'])

    # Compute the vocab size from the
    # torch text vocab instance.

    vocab_size = len(text_vocab.itos)

    # Create a batch generator for each data set.

    (train_loader, val_loader, test_loader) = build_data_batches(
        datasets, device=device, batch_size=configs['batch_size'])

    # Create an LSTM model instance..
    
    model = LSTM(
        vocab_size = vocab_size,
        embedding_dim = configs['embedding_dim'],
        hidden_dim = configs['hidden_dim'],
        output_dim = configs['output_dim'],
        bidirectional = configs['bidirectional'],
        dropout_linear = configs['dropout_linear'],
        dropout_rnn = configs['dropout_rnn'],
        save_dir=args.save_model_dir
        )

    # Train the model model using the batch generators.
    # Add a pre-trained word embedding is requested.

    if configs['pre_trained_emb']:
        pre_trained_embeddings = text_vocab.vectors
        # make sure emb dim is equal to 100 to match 
        # pre-trained network.
        assert model.embedding_dim == 100, """If use the pre-trained embeddings 
                the size of the embedding save must be 100-dim"""
        model.embed.weight.data.copy_(pre_trained_embeddings)
        if configs['freeze_pre_trained_layer']:
            model.embed.weight.requires_grad = True

    model_trainer = ModelTrainer(model, train_loader, val_loader, device, configs)
    model, history_train, history_val = model_trainer.train()

    # Save the model. To predict new text sequences
    # we also need to save the text and label vocabs
    # we will attach these to the model state.

    extra_state_info = {
        'text_vocab' : text_vocab,
        'label_vocab' : label_vocab
    }

    model.save_state(extra_state_info)


    # Create a plot of the training history
    plot_training_history(history_train, history_val)

    # Create predictions for the test set
    # and dump results.

    predict_test_set(model,test_loader,
                     label_vocab = label_vocab,
                     text_vocab=text_vocab,
                     text_label=configs['text_label'],
                     sentiment_map=sentiment_map,
                     device=device,
                     data_dir=args.data_root)


if __name__ == '__main__':

    # Load the input params
    args = parse_arguments()

    # Load the model params
    with open(args.config, 'r') as stream:
        configs = yaml.load(stream)

    # Setup logger
    logging.basicConfig(
        level=logging.INFO,
        format="%(asctime)s %(message)s",
        handlers=[
            logging.FileHandler(args.log_dir),
            logging.StreamHandler()]
    )

    # Set the correct device
    if args.gpu:
        device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
        print('Using device {}'.format(device))
    else:
        device = torch.device("cpu")
        print('Using device {}'.format(device))

    main(args, configs, device)


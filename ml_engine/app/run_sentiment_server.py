
import sys
sys.path.append('..')

from ml_engine.src.utils import load_model_checkpoint
from ml_engine.src.predictions import predict_raw_string
import argparse
import yaml
import flask
import torch

app = flask.Flask(__name__)

# Make these objects global
model = None
sentiment_map = None
text_vocab = None
label_vocab = None


def argument_parser():
    parser = argparse.ArgumentParser(
        description='Start REST API Server for Sentiment Prediction',)
    parser.add_argument('--checkpoint_path', type=str,
                        help='path to the saved model',
                        default='../models/checkpoint.pt')
    parser.add_argument('--sentiment_map_path', type=str,
                        help='path to the sentiment_map',
                        default='../data/sentiment_map.yml')
    parser.add_argument('--gpu', type=bool, default=True,
            help='Whether or not to use the GPU.')
    args = parser.parse_args()
    return args


@app.route("/predict", methods=["POST"])
def predict():

    data = flask.request.get_json(force=True)
    raw_text = data['text']

    ## make the predictions.
    predictions = predict_raw_string(model,
                                     raw_text,
                                     label_vocab,
                                     text_vocab,
                                     sentiment_map,
                                     device)

    return flask.jsonify(predictions)


if __name__ == '__main__':

    # Unpack arguments
    args = argument_parser()
    checkpoint_path = args.checkpoint_path
    sentiment_map_path = args.sentiment_map_path

    # Set the correct device
    if args.gpu:
        device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
        print('Using device {}'.format(device))
    else:
        device = torch.device("cpu")
        print('Using device {}'.format(device))

    print ("Loading pytorch model and data vocabs.")

    # The model is loaded here to avoid loading the
    # model into memory every time the rest API is called.
    # This also applies to the vocab objects, which could
    # be quite large.

    # Load the sentiment map:
    with open(sentiment_map_path, 'r') as stream:
        sentiment_map = yaml.load(stream)

    print ("Loading the sentiment map..")

    model, text_vocab, label_vocab = load_model_checkpoint(
        path=checkpoint_path, device=device)

    model.to(device)

    print ("Starting Flask server...")
    app.run()


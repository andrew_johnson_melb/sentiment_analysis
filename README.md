

# Sentiment Analysis of Text Data.

This project trains a Long short-term memory model to predict the sentiment 
of restaurant reviews. Each review in
the training data has been given a label 0 (indicating a negative
review) or 1 (indicating a positive review). Predicting the sentiment of a review is, 
therefore, a binary classification problem. 
The provided data consists of ~800 restaurant reviews. 

## The Dataset: Observations

We make the following important observations from the 
provided training data:

* **The amount of training data is very limited.**

As a result, the generalization error of the model will be difficult 
to estimate accurately. In addition, the lack of data motivates the use 
of transfer learning when building the model.

* **There is no a significant class imbalance.**

Importantly, the dataset is very evenly distributed between
positive and negative classes. Therefore, the ```accuracy``` metric will 
be sufficient to measure the performance of the estimator. 
If the class imbalance was significant one should consider the 
alternative  metrics such as ```precision```, ```recall```, 
and ```f1_weighted```.


## Pre-processing Text Data

Machine learning models expect numeric data, not raw text.
Briefly, to convert the raw text into numeric data we follow 
the steps below (this processes in called text Vectorization):

1. Special characters are removed from the text
2. The text is tokenized using 'spacy' <https://spacy.io/>
3. A vocabulary is built using pytorch and the text data is converted into a sequence of integers
4. The data is split into batches and the sequences are pad

## Motivations For the Model

* **The data is sequential.**

The best deep learning models for analyzing text are recurrent 
neural networks. They are particularly good at modelling sequences 
as they retain a memory of the previous elements in the sequence
(feed-forward networks consider each element/feature separably).
The state or memory of the previous words/tokens are maintained 
by a hidden state of the network. 

We will adopt a version of RNN architecture called Long Short-Term Memory.
LSTM networks are much more efficient at maintaining long-term information,
relative to standard RNN.

* **The training data is very limited.**

The most efficient way to represent words in neural networks is to use
word embeddings. In contract to one-hot-encodings, 
word embeddings represent words as dense low-dimensional vectors. 
Word embeddings are particularly powerful as the vectors can preserve 
semantic relationships between words.

Generally, context-specific word embeddings will
outperform pre-trained word embeddings (transfer learning is more powerful
for CNN networks). However, when the training data is very limited 
pre-trained word embeddings can improve the results.
Therefore, we will adopt the embeddings ```glove.6B.100d```,
see <https://nlp.stanford.edu/projects/glove/>

LSTM networks can process information from the start-to-end or end-to-start. 
The idea behind Bidirectional LSTMs is to increase the amount of input information 
into the network. Therefore, as the provided dataset is limited in size, we will 
implement a Bidirectional LSTM.
  
## Generating Training and Validation Data    
 
For simplicity, to estimate the generalisation 
error of the model we randomly divided the 
data into 80% training data and 20% validation data. 

Note, given the small size of the dataset, K-fold cross validation would 
give a more accurate result (see the further improvements 
section below).

## Model Architecture

Below we briefly describe the structure of the model.

1. **Word Embeddings**

First, a review is passed into the network as a sequence of integers. 
The embedding layer then maps each integer into a dense 100-dimensional vector.
With the option '''pre_trained_emb''' set to True, the embedding vectors 
are initialized to the values found in the '''glove.6B.100d''' dataset. Additionally,
when '''freeze_pre_trained_layer''' is set to True, the weights in the 
embedding layer are fixed and not updated as the network is trained.

2. **LSTM Network**

The output of the embeddings layer is passed into an LSTM network.
The LSTM network processes the input reviews word-by-word 
and builds up a memory of the previous states 
using a ```cell_state``` and ```hidden_state```.
Under the default options, the network analyses 
the input review from state-to-finish and finish-to-start.
This is called a Bidirectional LSTM network.

3. **Fully Connected Linear Layer**

The output from the LSTM is passed into a fully connected
linear layer. To make the output of the model more numerically stable we do 
not normalize it using a sigmoid transformation (this output is known as logits).
 Because of this choice and because this is a binary classification problem, we use 
 the ```BCEWithLogitsLoss()``` loss to train the model. This loss combines a 
 Sigmoid layer and the Binary Cross Entropy into a single function.

## Tuning the Model.

The model hyper-parameters were tuned based on the validation accuracy 
and loss. The following observations were made:

* Using pre-trained word embeddings significantly improved the performance.
  Furthremore, the embedding weights should be fixed.

* The validation accuracy of the final model reaches ~ 75%.

* The linear dropout layer is important for preventing over-fitting.

* A large number of epochs (>20) are required for training the model.

* Using a LSTM network improves the validation accuracy 
  relative to a basic RNN network.

## Assumptions

The major assumption made in this project is that the provided data represents a 
diverse set of reviews. Without enough diversity, the model will be 
unable to generalize to new unseen reviews, and thus it's practical 
applications will be very limited. Specifically, if the reviews are all 
of a similar nature (e.g., they use very similar vocabulary), 
the validation error will be a very bad estimate of the model's generalization
 error and the generalization error will be very large.

# Install

The dependencies can be installed using conda and the 
provided `enviroment.yml` file.

```bash
$ conda env create -f enviroment.yml
```

To activate the environment run 

```bash
$ source activate sentiment_env
```

Inside the environment, you may need to run 

```bash
$ python -m spacy download en
```
and due to a recent error in ```msgpack```
```bash
pip install msgpack==0.5.6
```

## Tests

To run integrations tests run

```bash
$ python -m pytest
```
Note, the integration tests should be run only on a **gpu** (otherwise they will be painfully slow.)

# Training the model

To train the model run

```
$ python train.py
```

# Generating predictions

To generate predictions on new text run

```bash
$ python predict --text "That's right....the red velvet cake.....ohhh this stuff is so good"
```

To generate predictions using a Rest API.

First, start the server using

```bash
$ python app/run_sentiment_server.py 
```

Then call the rest API using curl, e.g., 

```bash
$ curl -X POST -d '{"text" : "Thats right....the red velvet cake.....ohhh this stuff is so good."}' http://127.0.0.1:5000/predict
```

# Setting the model architecture

Within `./config/config.yml` the following parameters can be updated

```yaml
# Defines the dimensions of the dense
# word embeddings. When using the pre-trained
# word embedding network this value must be 100.
embedding_dim : 100

# The output dimensions of the LSTM network.
hidden_dim : 64

# The output dimensions of the network. For
# binary classification this value should be 1
output_dim: 1

# The dropout probability for the RNN.
# Note, this is only valid when stacking LSTM
# layers otherwise it is ignored.
dropout_rnn : 0.3

# The dropout probability for the final linear
# layer of the sentiment analysis network.
dropout_linear : 0.5

# bool, whether or not to use a bi-directional
# LSTM network.
bidirectional : True

# bool, whether or not to use pre-trained work
# embeddings to initialize the word embedding weights.
# Given the small data size this is highly
# recommended.
pre_trained_emb : True

# bool, whether or not to freeze the embedded layer.
# only valid when 'pre_trained_emb' is True
# Given the small data size this is highly
# recommended.
freeze_pre_trained_layer : True

# the name of the pretrained word
# embedding layer.
pretrained_emb_file : glove.6B.100d

```

# Setting the training parameters

Within `./config/config.yml` the following parameters can be updated

```yaml
# The learning rate for training the network.
learning_rate : 0.00005

# The number of epochs to train the network on
# Recommend 40
num_epochs : 40

# The size of the batches to use when training
# the network
batch_size : 32


```

# Further improvements

There are a number of improvements that could be made to this application.

* **Test Set predictions should use the full dataset.**

Currently only 80% of the data is used to train the final model and 
make predictions on the test set. This is clearly sub-optimal.

* **Accurately measure the generalization error.**

To tune hyper-parameters one should use cross-validation to measure 
the generalization error. This is important because of the size of the 
provided dataset. The small size means that the validation error will 
likely vary significantly between random divisions of the dataset.

* **Stacking LSTM layers**

LSTM models can be stacked on top of each other. Yet, for the model 
presented here we only consider a single layer.

* **More Context Specific Word Embeddings**

* Sentiment classification is a classic deep learning problem. As such, 
there are a lot of public datasets. The word embeddings trained using these 
datasets could be more tailored to the task at hand. As an example, 
one could try using the IMDB dataset.

# Production Ready

To make this code production ready the following changes
should be made:

*  Full integration and unit tests should be added.

* The rest API will not scale with a large number of incoming requests. 
  One option is to split incoming request into batches using Redis,
  For an example of this see    <https://www.pyimagesearch.com/2018/01/29/scalable-keras-deep-learning-rest-api/>
  
* Dockerizing the application.

# Why PyTorch

1. Pytorch supports dynamic computation graphs which makes it very
easy to develop and debug models (as compared to TensorFlow). 

2. Keras is a great ML API. However, it is a very high-level framework which makes implementing non-standard models difficult.

# References

For an introduction to RNNs see

<http://colah.github.io/posts/2015-08-Understanding-LSTMs/>

<http://karpathy.github.io/2015/05/21/rnn-effectiveness/>





